import { render } from "@testing-library/react";
import React from "react";
import TestAxios from "./TestAxios"
import '@testing-library/jest-dom/extend-expect';

it("should display a loading text", () => {
    const {getByTestId} = render(<TestAxios/>)

    expect(getByTestId("loading")).toHaveTextContent("Loading...")

})

it("should load and display data", async () => {

})