import React from "react";
import {shallow} from "enzyme";
import Counter from "./Counter";
import {fireEvent, render} from "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';


describe ("Basic rendering of Counter", () => {
    it("Should render Counter value", () => {
        // following arrange, act, assert here
        const counter = shallow(<Counter/>);

        // checks for any kind of text element
        const counterValue = counter.find(Text);

        expect(counterValue).toBeDefined();
    })

    it("Should be equal to 0", () => {
        const component = render(<Counter/>);

        expect(component.getByTestId("Counter")).toHaveTextContent(0);
    });

    it("Should be able to increment", () => {
        const {getByTestId} = render(<Counter/>);

        expect(getByTestId("up-button")).not.toHaveAttribute("disabled");
    })

    it("Should be able to decrement", () => {
        const {getByTestId} = render(<Counter/>);

        expect(getByTestId("down-button")).not.toHaveAttribute("disabled")
    })
})

describe ("Testing counter functionality", () => {
    it("increments the counter when Increment button is pressed", () => {
        const {getByTestId} = render(<Counter/>);

        fireEvent.click(getByTestId("up-button"));

        expect(getByTestId("Counter")).toHaveTextContent("1")
    })
    it("decrements the counter when Decrement button is pressed", () => {
        const {getByTestId} = render(<Counter/>);

        fireEvent.click(getByTestId("down-button"));

        expect(getByTestId("Counter")).toHaveTextContent("-1")
    })
})
