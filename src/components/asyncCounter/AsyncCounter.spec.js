import { render, fireEvent, waitFor } from "@testing-library/react";
import React from "react";
import AsyncCounter from "./AsyncCounter"
import '@testing-library/jest-dom/extend-expect';

describe("Testing asynchronous counter functionality", ()=>{
    it("increments the counter value .5 secs after increment button is clicked", async () => {
        const {getByTestId, getByText} = render(<AsyncCounter/>);

        fireEvent.click(getByTestId("up-button"));

        const counter = await waitFor(() => getByText("1"));

        expect(counter).toHaveTextContent("1");
    })
})